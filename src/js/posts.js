"use strict"

const itemPostsArray = [
    {
        title: 'Aenean Adipiscing Etiam Vestibulum',
        img: '../img/posts/post_1.jpg',
        alt: 'post_1',
        span: ['Photography, Journal','9 Comments'],
        description: `IEtiam porta sem malesuada euismod. Aenean leo quam. 
        Pellentesque ornare sem lacinia quam venenatis vestibulum.`,
    },
    {
        title: 'Aenean Adipiscing Etiam Vestibulum',
        img: '../img/posts/post_2.jpg',
        alt: 'post_2',
        span: ['Photography, Journal','9 Comments'],
        description: `IEtiam porta sem malesuada euismod. Aenean leo quam. 
        Pellentesque ornare sem lacinia quam venenatis vestibulum.`,
    },
    {
        title: 'Aenean Adipiscing Etiam Vestibulum',
        img: '../img/posts/post_3.jpg',
        alt: 'post_3',
        span: ['Photography, Journal','9 Comments'],
        description: `IEtiam porta sem malesuada euismod. Aenean leo quam. 
        Pellentesque ornare sem lacinia quam venenatis vestibulum.`,
    },
    {
        title: 'Aenean Adipiscing Etiam Vestibulum',
        img: '../img/posts/post_4.jpg',
        alt: 'post_4',
        span: ['Photography, Journal','9 Comments'],
        description: `IEtiam porta sem malesuada euismod. Aenean leo quam. 
        Pellentesque ornare sem lacinia quam venenatis vestibulum.`,
    },
]

const postsWrapper = document.querySelector('.posts__wrapper');
const postsArticle = document.createElement('article');
const postsArticleHTML = itemPostsArray.map((el) => `
    <artticle class="posts__item">
        <img src="${el.img}" alt="${el.alt}">
        <div>
            <h3>${el.title}</h3>
            <div class="posts__visited">
                <span>${el.span[0]}</span>
                <span>${el.span[1]}</span>
            </div>
            <p class="post__description">${el.description}</p>
            </div>
    </artticle>
    `)

    postsWrapper.insertAdjacentHTML('beforeend', postsArticleHTML.slice().join(''));