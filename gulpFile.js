import gulp from 'gulp';
import concat from 'gulp-concat';
import htmlmin from 'gulp-htmlmin';
import cleanCSS from 'gulp-clean-css';
import clean from 'gulp-clean';
import imagemin from 'gulp-imagemin';
// import image from 'gulp-image';
import minify from 'gulp-minify';
import autoprefixer from 'gulp-autoprefixer';
// import watch from 'gulp-watch';
import bs from 'browser-sync';
import gulpSass from "gulp-sass";
import nodeSass from "sass";
const sass = gulpSass(nodeSass);


const browserSync = bs.create();

const cleanDist = () => {
    return gulp.src('./dist', { read: false })
    .pipe(clean())
}

const html = () => {
    return gulp.src('./*.html')
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest("./dist"))
}

const convertCss = () => {
    return gulp.src('./src/scss/**/*.scss')
        .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(cleanCSS())
        .pipe(gulp.dest('./dist/styles'))
}

const js = () => {
    return gulp.src('./src/**/*.js')
        .pipe(concat('min.js'))
        .pipe(minify())
        .pipe(gulp.dest('./dist/scripts'))
}

const imgMin  = () => {
    return gulp.src('./src/images/**/*')
    .pipe(imagemin())
    .pipe(gulp.dest('./dist/img'))
}

const dev = () => {
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });

    gulp.watch('./src/**/*', gulp.series(cleanDist, gulp.parallel(html, js, convertCss, imgMin), (next) => {
        browserSync.reload();
        next();
    }))
}


gulp.task('build', gulp.series(cleanDist, gulp.parallel(html, js, imgMin, convertCss)));
gulp.task('dev', gulp.series('build', dev));

